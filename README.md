# E_MLG_LVI

Supplementary data files for Experimental study on the gradual fracture of layers in multi-layer laminated glass plates under low-velocity impact


- Data from sensors (trimmed)
- Photos of fracture patterns for individual samples
- High-speed camera photos for tested samples

Sample labels in manuscript vs files:
- 5LG-1 = 3-1
- 5LG-2 = 3-2
- 5LG-3 = 3-3
- 5LG-4 = 3-4
- 7LG-1 = 4-5
- 7LG-2 = 4-6
- 7LG-3 = 4-7
- 7LG-4 = 4-8

Position of sensors in manuscript vs files:
- C = Corner or Bottom_corner
- Q = Bottom_quarter
- M = Bottom or Bottom_center
- M' = Side
- I = Impactor
- (Center: only for two samples; sensors dropped off during measurements)


